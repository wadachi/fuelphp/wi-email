# Wadachi FuelPHP Email Package

An asynchronous transactional email service.

## メール　サービス　デーモンの使用方

### systemd

自動起動コマンド：
```bash
systemctl enable wi-email.service
```
メール　サービス　起動
```bash
systemctl start wi-email.service
```
メール　サービス　停止
```bash
systemctl stop wi-email.service
```

### sysv

自動起動コマンド：
```bash
chkconfig --add wi-email
```
メール　サービス　起動
```bash
service wi-email start
```
メール　サービス　停止
```bash
service wi-email stop
```
