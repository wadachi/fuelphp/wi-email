<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 依存性注入コンテナの設定
 */
return [
  'service.email' => [
    'class' => 'Wi\\Service_Email',
    'methods' => [
      'set_queue_service' => ['service.queue'],
    ],
  ],
];
