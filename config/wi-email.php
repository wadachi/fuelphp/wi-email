<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * メール タスク 設定
 */
return [
	'check_interval' => 5, // 秒
	'pid_file' => 'email.pid',
	'queue_name' => 'email'
];
