<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * メール サービス
 */
class Service_Email extends Service implements Interface_Service_Email
{
  // コンテナでサービス名
  const _PROVIDES = 'service.email';

  // 依存関係
  const _DEPENDS = [
    'set_queue_service' => 'service.queue'
  ];

  // キャッシュ キー
  const CACHE_CHALLENGE = 'email-challenge-%s';

  // デフォルト設定
  const DEFAULT_QUEUE = 'email';

  // サービス
  protected $queue_service;

  // トランザクションの深さ
  private static $transaction_depth = 0;

  // コミットされていないメール
  private static $transaction_mail = [];

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    \Config::load('wi-email', true);
  }// </editor-fold>

  /**
   * キュー サービスを設定する
   *
   * @access public
   * @param Interface_Service_Email $service キュー サービス
   */
  public function set_queue_service(Interface_Service_Queue $service)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $this->queue_service = $service;
  }// </editor-fold>

  /**
   * トランザクションを開始する
   *
   * @access public
   */
  public static function start_transaction()// <editor-fold defaultstate="collapsed" desc="...">
  {
    static::$transaction_depth++;
  }// </editor-fold>

  /**
   * トランザクションをコミットする
   *
   * @access public
   */
  public static function commit_transaction()// <editor-fold defaultstate="collapsed" desc="...">
  {
    static::$transaction_depth = max(static::$transaction_depth - 1, 0);

    if (static::$transaction_depth === 0)
    {
      $queue = \Config::get('wi-email.queue_name', self::DEFAULT_QUEUE);
      $service = Container::get('service.queue');

      foreach (static::$transaction_mail as $task)
      {
        $service->push($queue, $task);
      }
    }
  }// </editor-fold>

  /**
   * トランザクションをロールバックする
   *
   * @access public
   */
  public static function rollback_transaction()// <editor-fold defaultstate="collapsed" desc="...">
  {
    static::$transaction_depth = 0;
    static::$transaction_mail = [];
  }// </editor-fold>

  /**
   * チャレンジ コードを作成する
   *
   * @access public
   * @param string $challenge チャレンジ コード
   */
  protected function create_challenge_code($response, $expiration)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $challenge = bin2hex(openssl_random_pseudo_bytes(4));
    $cache_key = vsprintf(self::CACHE_CHALLENGE, $challenge);
    \Cache::set($cache_key, $response, $expiration);

    return $challenge;
  }// </editor-fold>

  /**
   * チャレンジ コードを削除する
   *
   * @access public
   * @param string $challenge チャレンジ コード
   */
  public function delete_challenge_code($challenge)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (empty($challenge))
    {
      return false;
    }

    $cache_key = vsprintf(self::CACHE_CHALLENGE, $challenge);

    try
    {
      \Cache::delete($cache_key);
    }
    catch (\CacheNotFoundException $e)
    {
      \Log::debug($e);
    }
  }// </editor-fold>

  /**
   * チャレンジ コードでチャレンジ レスポンスを取得する
   *
   * @access public
   * @param string $challenge チャレンジ コード
   * @return array レスポンスが有効である場合はレスポンス、そうでない場合はfalse
   */
  public function get_challenge_response($challenge)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (empty($challenge))
    {
      return false;
    }

    $cache_key = vsprintf(self::CACHE_CHALLENGE, $challenge);

    try
    {
      return \Cache::get($cache_key);
    }
    catch (\CacheNotFoundException $e)
    {
      \Log::debug($e);
      return false;
    }
  }// </editor-fold>

  /**
   * チャレンジ コードを生成してメールを送る
   *
   * @access public
   * @param string $address メールアドレス
   * @param mixed $config メール設定 （配列か、文字列Configパース）
   * @param string $response チャレンジ レスポンス
   * @param array $data テンプレート データ
   * @return string チャレンジ コード
   */
  public function send_challenge_code($address, $config, $response, array $data = [])// <editor-fold defaultstate="collapsed" desc="...">
  {
    $config = is_string($config) ? \Config::get($config) : $config;
    $challenge = $this->create_challenge_code($response, \Arr::get($config, 'expiration'));
    $this->send_mail($address, $config, array_merge($data, ['_code' => $challenge]));

    return $challenge;
  }// </editor-fold>

  /**
   * 非同期的にメールを送る
   *
   * @access public
   * @param string $address メールアドレス
   * @param mixed $config メール設定 （配列か、文字列Configパース）
   * @param array $data テンプレート データ
   */
  public function send_mail($address, $config, array $data = [])// <editor-fold defaultstate="collapsed" desc="...">
  {
    $config = is_string($config) ? \Config::get($config) : $config;

    $task = [
      'address' => $address,
      'config' => $config,
      'data' => $data
    ];

    if (static::$transaction_depth !== 0)
    {
      static::$transaction_mail[] = $task;
    }
    else
    {
      $queue = \Config::get('wi-email.queue_name', self::DEFAULT_QUEUE);
      $this->queue_service->push($queue, $task);
    }
  }// </editor-fold>
}
