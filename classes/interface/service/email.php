<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * メール サービス インタフェース
 */
interface Interface_Service_Email
{
  /**
   * チャレンジ コードを削除する
   *
   * @access public
   * @param string $challenge チャレンジ コード
   */
  function delete_challenge_code($challenge);

  /**
   * チャレンジ コードでチャレンジ レスポンスを取得する
   *
   * @access public
   * @param string $challenge チャレンジ コード
   * @return array レスポンスが有効である場合はレスポンス、そうでない場合はfalse
   */
  function get_challenge_response($challenge);

  /**
   * チャレンジ コードを生成してメールを送る
   *
   * @access public
   * @param string $address メールアドレス
   * @param mixed $config メール設定 （配列か、文字列Configパース）
   * @param string $response チャレンジ レスポンス
   * @param array $data テンプレート データ
   * @return string チャレンジ コード
   */
  function send_challenge_code($address, $config, $response, array $data = []);

  /**
   * 非同期的にメールを送る
   *
   * @access public
   * @param string $address メールアドレス
   * @param mixed $config メール設定 （配列か、文字列Configパース）
   * @param array $data テンプレート データ
   */
  function send_mail($address, $config, array $data = []);
}
