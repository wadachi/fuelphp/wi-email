<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Package::load([
  'wi-container',
  'wi-queue',
]);

\Autoloader::add_classes([
  'Wi\\Interface_Service_Email' => __DIR__.'/classes/interface/service/email.php',
  'Wi\\Service_Email' => __DIR__.'/classes/service/email.php',
]);
