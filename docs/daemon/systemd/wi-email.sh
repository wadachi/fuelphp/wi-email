#!/bin/sh
#
# メール　サービス
#
# ファイル場所：
#   /usr/lib/systemd/scripts/wi-email.sh
# ファイル権限：
#   chown root:root /usr/lib/systemd/scripts/wi-email.sh
#   chmod 755 /usr/lib/systemd/scripts/wi-email.sh
# 自動起動コマンド：
#   systemctl enable wi-email.service

start() {
  exec nice -n 10 $php $work_dir/oil r email
}

stop() {
  if [ -f $pid_file ]; then
    kill $(<$pid_file)
  fi
}

restart() {
  stop
  start
}

flush() {
  exec $php $work_dir/oil r email:flush
}

php=/usr/bin/php
work_dir=/home/fuelphp
pid_file="$work_dir/email.pid"

export FUEL_ENV=production

case $1 in
  start|stop|restart|flush) "$1" ;;
esac

exit 0
