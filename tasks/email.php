<?php
/**
 * Wadachi FuelPHP Email Package
 *
 * An asynchronous transactional email service.
 *
 * @package    wi-email
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Tasks;

\Package::load('email');
\Package::load('wi-container');
\Package::load('wi-queue');

use \Arr;
use \Fuel;
use \Fuel\Core\Cli;
use \Fuel\Core\Config;
use \Fuel\Core\File;
use \Fuel\Core\Log;
use \Fuel\Core\View;
use \Wi\Container;

/**
 * メール タスク
 */
class Email
{
  // デフォルト設定
  const DEFAULT_INTERVAL = 5;
  const DEFAULT_PID_FILE = 'wi-email.pid';
  const DEFAULT_QUEUE = 'wi-email';

  // メールキュー
  protected static $queue_name = null;
  protected static $queue_service = null;

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    Config::load('wi-email', true);
    static::$queue_name = Config::get('wi-email.queue_name', self::DEFAULT_QUEUE);
    static::$queue_service = Container::get('service.queue');
  }// </editor-fold>

  /**
   * メール処理を実行する
   * 実行の方法：php oil r WiEmail
   *
   * @access public
   */
  public static function run()// <editor-fold defaultstate="collapsed" desc="...">
  {
    // 診断法
    Cli::write('FUEL_ENV: '.Fuel::$env);
    Cli::write('Queue: '.static::$queue_name);

    // デーモン処理の確認
    $pid_dir = Config::get('wi-email.pid_dir', DOCROOT);
    $pid_file = Config::get('wi-email.pid_file', self::DEFAULT_PID_FILE);

    if (File::exists($pid_dir.$pid_file))
    {
      Cli::error('PID file already exists: '.$pid_dir.$pid_file);
      return;
    }

    // デーモン化
    $pid = pcntl_fork();

    if ($pid)
    {
      Cli::write('Daemon started');
      return 0;  // 親プロセスを終了
    }

    File::create($pid_dir, $pid_file, getmypid()."\n");

    register_shutdown_function(function() use ($pid_dir, $pid_file)
    {
      File::exists($pid_dir.$pid_file) and File::delete($pid_dir.$pid_file);
    });

    // デーモンの信号処理
    $exit = false;
    $on_exit = function() use (&$exit) { $exit = true; };

    foreach([SIGINT, SIGQUIT, SIGHUP, SIGTERM] as &$sig)
    {
      pcntl_signal($sig, $on_exit);
    }

    // メール ループ
    $interval = +Config::get('wi-email.check_interval', self::DEFAULT_INTERVAL);

    while(!$exit)
    {
      if (!static::send_one())
      {
        sleep($interval);
      }

      pcntl_signal_dispatch();
    }

    return 0;
  }// </editor-fold>

  /**
   * メールキューを空にする
   * 実行の方法：php oil r WiEmail:flush
   *
   * @access public
   */
  public static function flush()// <editor-fold defaultstate="collapsed" desc="...">
  {
    static::$queue_service->flush(static::$queue_name);
  }// </editor-fold>

  /**
   * メールキューから1つのメールを送信する
   * 実行の方法：php oil r WiEmail:send_one
   *
   * @access public
   * @return bool メールキューがメッセージを含む場合はtrue、そうでない場合はfalse
   */
  public static function send_one()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $task = static::$queue_service->pop(static::$queue_name);

    if (empty($task))
    {
      return false;
    }

    try
    {
      $email = \Email::forge();
      $email->from(Arr::get($task, 'config.from.email'), Arr::get($task, 'config.from.name'));
      $email->to(Arr::get($task, 'address'));
      $email->subject(Arr::get($task, 'config.subject'));
      $email->html_body(View::forge(Arr::get($task, 'config.template'), Arr::get($task, 'data')));
      $email->send();
    }
    catch (\Exception $e)
    {
      Log::debug($e);
    }

    return true;
  }// </editor-fold>
}
